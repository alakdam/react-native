import React, { useState, useEffect } from "react";
import { StyleSheet, Text, View, ScrollView, Image } from "react-native";

export default function App() {
  const [state, setstate] = useState([] as any[]);

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    const response = await fetch("https://jsonplaceholder.typicode.com/photos");
    const data = await response.json();
    setstate(data); //i change the shit
  };

  return (
    <ScrollView style={styles.body}>
      <View style={styles.container}>
        {state.map(img => {
          return (
            <View key={img.id}>
              <Image style={styles.stretch} source={{ uri: img.url }} />
            </View>
          );
        })}
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  body: {
    padding: 150
  },
  container: {
    flex: 1,
    backgroundColor: "black",
    alignItems: "center",
    justifyContent: "center"
  },

  stretch: {
    width: 200,
    height: 200,
    resizeMode: "stretch"
  }
});
